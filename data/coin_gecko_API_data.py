from pycoingecko import CoinGeckoAPI
from datetime import datetime
import numpy as np
import pandas as pd 
import time 

#this is for crypto data 


cg = CoinGeckoAPI() 

bitcoin_USD_price = cg.get_price(ids='bitcoin', vs_currencies='usd')
the_graph_USD_price = cg.get_price(ids='the-graph', vs_currencies='usd')


test = cg.get_coins_list() #in alphabetical order 


coin_name_list = [] 
for coin in test:
    coin_name_list.append(coin['id'])


btc_stats = cg.get_price(ids='bitcoin', vs_currencies='usd', include_market_cap='true')
#print('usd market cap',btc_stats['bitcoin']['usd_market_cap'])

#grab_data = input("put in the id of the currency you choose ")
#historical_data_grab = cg.get_coin_market_chart_by_id(id=grab_data,vs_currency='usd',days = 1)

btc_historical = cg.get_coin_market_chart_by_id(id='bitcoin',vs_currency='usd',days = 1)
grt_historical = cg.get_coin_market_chart_by_id(id='the-graph',vs_currency='usd',days = 1) #very granular data 




def data_cleanup(historical): #turns json response into a numpy dataframe 

    prices_timestamp_vals = historical['prices'] #returns the market cap for the associated time stamp 
    market_caps_timestamp_vals = historical['market_caps']
    #total_volumes_timestamp_vals = historical['total_volumes']
   
    #dataframe columns 
    timestamps = []
    for val in market_caps_timestamp_vals: #getting timestamps 
        test = datetime.utcfromtimestamp(val[0]/1000).strftime('%Y-%m-%d %H:%M:%S')
        timestamps.append(test)
    caps = []
    for cap in market_caps_timestamp_vals:
        caps.append(cap[1])
    prices = []
    for price in prices_timestamp_vals:
        prices.append(price[1])

    timestamps_array = np.array(timestamps)
    caps_array = np.array(caps)
    prices_array = np.array(prices)
    test = {'timestamps':timestamps_array,'market_caps': caps_array,'prices':prices_array }
    wb = pd.DataFrame(data=test)
    return wb

test = data_cleanup(grt_historical)
print('specialist JSON to CSV converter')
print('1 day GRT data')
write_choice = input('write to csv? (Y/n)').upper()
if write_choice == 'Y':
    filename = input("Write filename:")
    test.to_csv(filename)
elif write_choice == 'N':
    pass
