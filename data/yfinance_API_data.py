import yfinance as yf #https://pypi.org/project/yfinance/ 
import numpy as np
import pandas as pd 

#this is for stocks data 

msft = yf.Ticker("MSFT")
#ticker module allows for ticker data access


hist = msft.history(period="max")
print(hist)
#returns as a pandas dataframe with OHLCV

