# Quant Project

Algorithmic trading project

The project will be split into three main parts

1. Data clean up 
- data should be stored in data directory


2. Hypothesis formulation

- analysis to be held in analysis directory 
- analysis to be performed in jupyter notebooks for clarity 


3.  Hypothesis testing (using data from step 1.)
- backtesting to be held in backtesting directory (tbd)


Done so far
- Sorted out access for realtime crypto data, just import the relevant modules, documentation tbd. 
- Sorted out access for some historical stock market data 
- Sorted automatically updating list of S&P500 companies  
- Done a bunch of macroeconomic visualisation - the final aim with which is to make a visual calculator
